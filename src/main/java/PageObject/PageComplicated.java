package PageObject;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.io.FileUtils;

public class PageComplicated {
    @FindBy(xpath = "//a[@class=\"et_pb_button et_pb_button_0 et_pb_bg_layout_light\"]")
    private WebElement Button0;

    @FindBy(xpath = "//input[@id=\"et_pb_contact_name_0\"]")
    private WebElement NameField;

    @FindBy(xpath = "//input[@id=\"et_pb_contact_email_0\"]")
    private WebElement MailField;

    @FindBy(xpath = "//textarea[@id=\"et_pb_contact_message_0\"]")
    private WebElement MessageField;

    @FindBy(xpath = "//p[@class=\"et_pb_contact_field et_pb_contact_field_2 et_pb_contact_field_last\"]/following-sibling::div//input")
    private WebElement ResultatField;

    @FindBy(xpath = "//p[@class=\"et_pb_contact_field et_pb_contact_field_2 et_pb_contact_field_last\"]/following-sibling::div//button")
    private WebElement SubmitButton;

    @FindBy(xpath = "//p[contains(text(),\"Thanks for contacting us\")]")
    public WebElement SuccessMessage;

    @FindBy(xpath = "//p[@class=\"et_pb_contact_error_text\"]")
    public WebElement FailMessage;

    public void ClicButton() {
        Button0.click();
    }

    public void RemplirFormulaire(String name, String Email, String Message) throws InterruptedException {
        NameField.clear();
        NameField.sendKeys(name);
        MailField.clear();
        MailField.sendKeys(Email);
        MessageField.clear();
        MessageField.sendKeys(Message);
        int Number1 = Integer.parseInt(ResultatField.getAttribute("data-first_digit"));
        int Number2 = Integer.parseInt(ResultatField.getAttribute("data-second_digit"));
        int Result = Number1 + Number2;
        String ResultToType = String.valueOf(Result);
        ResultatField.sendKeys(ResultToType);
        Thread.sleep(500);
        SubmitButton.click();
    }

    public static void takeSnapShot(WebDriver webdriver, String fileWithPath) throws Exception {
        // Convert web driver object to TakeScreenshot
        TakesScreenshot scrShot = ((TakesScreenshot) webdriver);
        // Call getScreenshotAs method to create image file
        File SrcFile = scrShot.getScreenshotAs(OutputType.FILE);
        // Move image file to new destination
        File DestFile = new File(fileWithPath);
        // Copy file at destination
        FileUtils.copyFile(SrcFile, DestFile);
    }

    public static String getNameFile() {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd 'at' HH:mm:ss z");
        Date date = new Date(System.currentTimeMillis());
        String dateNow = formatter.format(date);
        String fileName = dateNow.toString().replace(":", "_").replace(" ", "_") + ".png";
        return fileName;
    }
}

