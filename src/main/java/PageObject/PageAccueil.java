package PageObject;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class PageAccueil {
    @FindBy (xpath="//a[contains(text(),\"Big page with many elements\")]")
    private WebElement Big_Pages;

    @FindBy (xpath="//title")
    private WebElement Title_Accueil;

    public PageComplicated NavigateToComplicated(WebDriver driver) {
        Big_Pages.click();
        return PageFactory.initElements(driver, PageComplicated.class);
    }
}
