import PageObject.PageAccueil;
import PageObject.PageComplicated;
import org.junit.After;
import org.junit.Before;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.params.shadow.com.univocity.parsers.conversions.Conversions.toUpperCase;

public class UqaTest {
    WebDriver driver;
    WebDriverWait wait;
    Logger logger = LoggerFactory.getLogger(UqaTest.class);


    @BeforeEach
    public void setup() {
        //Initialisation du driver
        System.setProperty("webdriver.chrome.driver", "src/main/resources/driver/chromedriver.exe");
        driver = new ChromeDriver();
        //aller sur le site du Projet
        driver.get("https://ultimateqa.com/automation/");
        driver.manage().window().maximize();
        logger.info("***Le site Web est correctement chargé***".toUpperCase());
    }

    @AfterEach
    public void teardown() {
        driver.quit();
        logger.info("***fin du test***".toUpperCase());
    }


    @Test
    public void test() throws Exception {
        wait = new WebDriverWait(driver, Duration.ofSeconds(15));
        try {
            //initialiser page accueil
            PageAccueil page_accueil = PageFactory.initElements(driver, PageAccueil.class);
            assertEquals("Automation Practice - Ultimate QA", driver.getTitle());
            logger.info("***Page Accueil***".toUpperCase());
            //naviguer vers page "complicated"
            PageComplicated page_complicated = page_accueil.NavigateToComplicated(driver);
            assertEquals("Complicated Page - Ultimate QA", driver.getTitle());
            logger.info("***Page Complicated***".toUpperCase());
            //action sur la page "complicated"
            page_complicated.ClicButton();
            page_complicated.RemplirFormulaire("jules", "gee@gmail.com", "test OK");
            wait.until(ExpectedConditions.visibilityOf(page_complicated.SuccessMessage));
            assertTrue(page_complicated.SuccessMessage.isDisplayed());

        }catch(Exception e){
                PageComplicated.takeSnapShot(driver, PageComplicated.getNameFile());
                logger.error("Exception while taking screenshot ".toUpperCase() + e.getMessage());
                throw e;
            }
        }
}
